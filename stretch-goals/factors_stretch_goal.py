def factors(number):
    factors = []
    for x in range(2,number):
        if number%x == 0:
            factors.append(x)
            
    if factors == []:
        factors = f"{number} is a prime number"
    return factors

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
