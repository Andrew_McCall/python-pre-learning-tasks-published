def calculator(a, b, operator):
    if operator == "+":
        return binary_converter(a+b)
    elif operator == "-":
        return binary_converter(a-b)
    elif operator == "*":
        return binary_converter(a*b)
    elif operator == "/":
        return binary_converter(int(a/b))
    
def binary_converter(denary):
    digits = 0
    while 2**(digits+1) < denary:
        digits += 1
    
    output = ''
    while digits >= 0:
        if denary - 2**digits >= 0:
            denary -= 2**digits
            output += '1'
        else:
            output += '0'
        digits -= 1
    
    return output

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
