cases = [('a','4'),('e','3'),('i','!'),('o','ooo'),('u','|_|')]

def vowel_swapper(string):
    output = list(string)
    for case in cases:
        index = string.lower().find(case[0])
        if index != -1:
            index = string.lower().find(case[0],index+1) ##finds second,
            if index != -1:
                if output[index] == 'O':
                    output.insert(index ,'000')
                else:
                    output.insert(index, case[1])
                    
                del output[index+1]
            
    return ''.join(output)

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
